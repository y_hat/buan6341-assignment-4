# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 13:09:52 2018

@author: Victor Faner

Assignment 4: 
"""

###############################################################################
#                               Library imports                               #
###############################################################################

import pandas as pd
import numpy as np
import sklearn.decomposition as decomp
from sklearn.mixture import GaussianMixture as GM
from sklearn.cluster import KMeans
from sklearn.neural_network import MLPClassifier
from sklearn import preprocessing as sk_prep
from sklearn import model_selection as sk_modsel
from sklearn import random_projection as sk_randproj
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import itertools
#from scipy import stats
from scipy import interp
from time import time
import graphviz

# Track code runtime
init = time()

###############################################################################
#                               Method Definitions                            #
###############################################################################

## Preprocessing steps
def prep(x, y, test_size=.3, scaler=sk_prep.StandardScaler(), 
           random_state=None, stratify=None):  
    
    # Convert categorical variables
    x = pd.get_dummies(x)     
    # Train-test split
    x_train, x_test, y_train, y_test = sk_modsel.train_test_split(x, y, 
                                            test_size=test_size,
                                            random_state=random_state,
                                            stratify=stratify)    
    # Feature scaling
    scaler = scaler
    scaler.fit(x_train)
    x_train = pd.DataFrame(scaler.transform(x_train), index=x_train.index, 
                           columns=x_train.columns)
    x_test = pd.DataFrame(scaler.transform(x_test), index=x_test.index, 
                          columns=x_test.columns) 
            # fit test set onto same scaling transformation as training set
    
    return x_train, x_test, y_train, y_test    

## Function to generate confusion matrices
def plot_confusion_matrix(cm, classes, normalize=False, 
                          title='Confusion matrix', cmap=plt.cm.Blues):
    
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print('Normalized confusion matrix')
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment='center',
                 color='white' if cm[i, j] > thresh else 'black')

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()    

## Function to plot parameter validation curves
def plot_validation_curve(x, y, model, title, param_name, param_range,
                          cv=sk_modsel.StratifiedKFold(n_splits=5, 
                                                       random_state=42),
                          layers=None, ylim=None):

    train_scores, test_scores = sk_modsel.validation_curve(model, x, y, cv=cv,
            param_name=param_name, param_range=param_range, scoring='accuracy')
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    plt.title(title)
    plt.xlabel(str(param_name))
    plt.ylabel('Score') 

    if layers is not None:
        param_range = [x[layers-1] for x in param_range]
        # index of layers for ANN
        plt.xlabel(str(param_name) + ' (layer ' + str(layers) + ')')
    
    if ylim is not None:
        plt.ylim(*ylim)
        
    lw = 2
    plt.plot(param_range, train_scores_mean, label='Training score',
             color='darkorange', lw=lw)
    plt.fill_between(param_range, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.2,
                     color='darkorange', lw=lw)
    plt.plot(param_range, test_scores_mean, label='Cross-validation score',
             color='navy', lw=lw)
    plt.fill_between(param_range, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.2,
                     color='navy', lw=lw)
    plt.legend(loc='best')
    plt.show()

## Function to plot training size learning curves  
def plot_learning_curve(model, title, x, y, ylim=None, 
                        cv=sk_modsel.StratifiedKFold(n_splits=5, 
                                                     random_state=42), 
                        n_jobs=None, 
                        train_sizes=np.linspace(.1, .7, 5)):
    
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel('Training examples')
    plt.ylabel('Score')
    train_sizes, train_scores, test_scores = sk_modsel.learning_curve(
        model, x, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color='r')
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color='g')
    plt.plot(train_sizes, train_scores_mean, 'o-', color='r',
             label='Training score')
    plt.plot(train_sizes, test_scores_mean, 'o-', color='g',
             label='Cross-validation score')

    plt.legend(loc='best')
    plt.show()
    return plt

## Function to plot ROC curves
def plot_roc_curve(x, y, model, title, pos_label, 
                   cv=sk_modsel.StratifiedKFold(n_splits=5, random_state=42)):
    
    x = x.values
    y = y.values
    
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    
    i = 0
    for train, test in cv.split(x, y):
        probas_ = model.fit(x[train], y[train]).predict_proba(x[test])
        # Compute ROC curve and area under the curve
        fpr, tpr, thresholds = metrics.roc_curve(y[test], probas_[:, 1],
                                                 pos_label=pos_label)
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = metrics.auc(fpr, tpr)
        aucs.append(roc_auc)
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
    
        i += 1
    plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
             label='Chance', alpha=.8)
    
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = metrics.auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')
    
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(title)
    plt.legend(loc="lower right")
    plt.show()
    
## Utility function to report best scores on paramater search
def param_report(results, n_top=3):
    
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print('Model with rank: {0}'.format(i))
            print('Mean validation score: {0:.3f} (std: {1:.3f})'.format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print('Parameters: {0}'.format(results['params'][candidate]))
            print('')

## Function to determine best-fitting parameters
def param_search(x, y, model, params, search_type, n_iter_search=10, cv=5, 
                 random_state=None, n_top=3):
    
    # Grid search
    if search_type == 'grid':
        search = sk_modsel.GridSearchCV(model, param_grid=params, cv=cv,
                                        iid=False)
    # Randomized search
    elif search_type == 'rand':
        search = sk_modsel.RandomizedSearchCV(model, cv=cv, iid=False,
                    param_distributions=params, n_iter=n_iter_search,
                    random_state=random_state)
        
    start = time()
    search.fit(x, y)
    
    results = search.cv_results_
    
    print(
    'Parameter Search took %.2f seconds for %d candidate parameter settings.'
          % (time() - start, len(results['params'])))
    
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print('Model with rank: {0}'.format(i))
            print('Mean validation score: {0:.3f} (std: {1:.3f})'.format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print('Parameters: {0}'.format(results['params'][candidate]))
            print('')  

### Function to determine optimal number of K-Means Clusters and plot results
def km_search(x, y, title='K-Means Elbow Test', k=range(1,11)):
    wss = []
    
    for i in k:
        model = KMeans(n_clusters=i, random_state=42)
        model.fit(x, y, sample_weight=sample_weight)
        wss.append(model.inertia_)
       
    plt.plot(k, wss, 'bs-')
    plt.xlabel('Number of Clusters')
    plt.ylabel('WSS')
    plt.title(title)
    plt.show() # 5 seems ideal   

### Function to determine optimal number of EM Clusters and plot results    
def em_search(x, y, title='EM BIC', k=range(1,11)):
    bic = []
    
    for i in k:
        model = GM(n_components=i, random_state=42)
        model.fit(x, y)
        bic.append(model.bic(x))
       
    plt.plot(k, bic, 'bs-')
    plt.xlabel('Number of Clusters')
    plt.ylabel('BIC')
    plt.title(title)
    plt.show() # 2 seems ideal
    
    print(min(bic))

def plot_silhouette(x, y, model, n_clusters, title):
    # Create a subplot with 1 row and 2 columns
    fig, (ax1) = plt.subplots(1)
    fig.set_size_inches(7, 7)
    
    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(x_train) + (n_clusters + 1) * 10])
    
    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    cluster_labels = model.fit_predict(x_train)
    
    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = metrics.silhouette_score(x_train, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)
    
    # Compute the silhouette scores for each sample
    sample_silhouette_values = metrics.silhouette_samples(x_train, cluster_labels)
    
    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]
    
        ith_cluster_silhouette_values.sort()
    
        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i
    
        color = cm.nipy_spectral(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)
    
        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples
    
    ax1.set_title(title)
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
    
    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
    plt.show()
    
### Function wrapper for ANN plots and other evaluations
def ann_eval(x_train, y_train, x_test, y_test, model):
    
    # Validation curves: hidden_layer_sizes, alpha
    title = 'ANN Validation Curve: 1 hidden layer'
    plot_validation_curve(x_train, y_train, model, title,
                          param_name='hidden_layer_sizes', 
                          param_range=np.arange(1,x_train.shape[1]+1,2))       
#    plot_validation_curve(x_train, y_train, model, title, param_name='alpha', 
#                          param_range=np.arange(1e-6,.1,5e-3)) 
#    
#    # learning curve
#    title = 'ANN Learning Curve: 1 hidden layer'
#    plot_learning_curve(model, title, pd.concat([x_train, x_test]), 
#                        pd.concat([y_train, y_test]))
#    
#    # ROC curve
#    title = 'ANN ROC: Passing class predictions (1 layer)'
#    plot_roc_curve(x_train, y_train, model, title, pos_label='Passing')
    
    # confusion matrix
    title = 'ANN Confusion Matrix: 1 hidden layer'
    model.fit(x_train, y_train)
    
    cnf_matrix = metrics.confusion_matrix(y_test, model.predict(x_test))
    np.set_printoptions(precision=2)
        
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=np.unique(y_train), 
                          normalize=False, title=title)
    
    plt.show()
    
    # Final model accuracy
    print('Training Set accuracy: ' + 
          str(metrics.accuracy_score(y_train, model.predict(x_train))))
    print('Test Set accuracy: ' + 
          str(metrics.accuracy_score(y_test, model.predict(x_test))))    
         
#### Utility function to generate possible hidden_layer_sizes for param_search
#def rSubset(arr, r, combinator=itertools.combinations()): 
#  
#    # return list of all subsets of length r 
#    # to deal with duplicate subsets use  
#    # set(list(combinations(arr, r))) 
#    return list(combinator(arr, r))
            
###############################################################################
#                               Analyses: Data Set 1                          #
###############################################################################

############################
# Data Set 1 - Preprocessing
############################

### Data set import and exploratory analysis
context1 = pd.read_csv('student-mat.csv',sep=';')
context1.head()
context1.describe()
context1 = context1.drop(['G1'], axis=1)
context1 = context1.drop(['G2'], axis=1)
context1['G3'] = np.where(context1['G3']>=10, 'Passing', 'Failing') 
                            # convert G3 to binary class label
print('Data Set 1')

### Preprocess
x_train, x_test, y_train, y_test = prep(context1.drop('G3', axis=1), 
                                          context1['G3'], random_state=42, 
                                          scaler=sk_prep.MaxAbsScaler(),
                                          stratify=context1['G3'], 
                                          test_size=.3,)

x_train = x_train.drop(['school_GP','sex_F','address_R','famsize_LE3',
                        'Pstatus_A','schoolsup_no','famsup_no','paid_no',
                        'activities_no','nursery_no','higher_no','internet_no',
                        'romantic_no','Mjob_other','Fjob_other','reason_other',
                        'guardian_other'], axis=1)
x_test = x_test.drop(['school_GP','sex_F','address_R','famsize_LE3',
                      'Pstatus_A','schoolsup_no','famsup_no','paid_no',
                      'activities_no','nursery_no','higher_no','internet_no',
                      'romantic_no','Mjob_other','Fjob_other','reason_other',
                      'guardian_other'], axis=1)
                        # removing additional binary variables

sample_weight = np.array([1 if i == 'Passing' else 2 for i in y_train])
    # for sample class weighting on .fit methods

######################################################
# Data Set 1 - Task 1 - Clustering on full feature set
######################################################

### K-Means - 1 to 15 clusters
km_search(x_train, y_train) # Elbow at 4 or 5 clusters  

## Store KM-clustered data set, first as dummies, then as single features
model = KMeans(n_clusters=5, random_state=42)
model = model.fit(x_train)

x_train_km = pd.DataFrame(model.labels_, index=x_train.index)
x_train_km = pd.get_dummies(x_train_km[0])
x_train_km = x_train_km.drop([4], axis=1)
x_test_km = pd.DataFrame(model.predict(x_test), index=x_test.index)
x_test_km = pd.get_dummies(x_test_km[0])
x_test_km = x_test_km.drop([4], axis=1)

title = 'K-Means Silhouette Plot'
plot_silhouette(x_train, y_train, model, 5, title)

### Expectation-Maximization - 1 to 10 clusters
em_search(x_train, y_train) # Lowest BIC at 2 clusters

## Store EM-clustered data set
model = GM(n_components=2, random_state=42)
model = model.fit(x_train)

x_train_em = pd.DataFrame(model.predict(x_train), index=x_train.index)
x_test_em = pd.DataFrame(model.predict(x_test), index=x_test.index)

title = 'Expectation Maximization Silhouette Plot'
plot_silhouette(x_train, y_train, model, 2, title)

################################################
# Data Set 1 - Task 2 - Dimensionality Reduction
################################################

### Decision Tree
model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train, y_train, sample_weight)
model.feature_importances_

## Store DT-reduced data set
x_train_dt = x_train.copy()
x_test_dt = x_test.copy()

i = 0

for column in x_train:
#    print(column + ' ' + str(model.feature_importances_[i]))
    if model.feature_importances_[i] <= .01: # only include features with 
                                            # >.01 importance
        x_train_dt = x_train_dt.drop([column], axis=1)
        x_test_dt = x_test_dt.drop([column], axis=1)
    i += 1

## Tree visualization
model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train_dt, y_train, sample_weight)

dot_data = tree.export_graphviz(model, out_file=None, max_depth=2, 
                     feature_names=x_train_dt.columns,  
                     class_names=y_train.values,  
                     filled=True, rounded=True,  
                     special_characters=True)  
graph = graphviz.Source(dot_data)  
graph 

### PCA
pca = decomp.PCA(random_state=42).fit(x_train)

plt.plot(np.cumsum(pca.explained_variance_ratio_), 'b--')
plt.xlabel('Number of Components')
plt.ylabel('Variance (%)') #for each component
plt.title('Explained Variance')
plt.show() # 22 components preserves >= 90% of variance
print(np.cumsum(pca.explained_variance_ratio_))

## Store PCA-transformed data set
pca = decomp.PCA(n_components=22, random_state=42).fit(x_train)
x_train_pca = pd.DataFrame(pca.transform(x_train), index=x_train.index)
x_test_pca = pd.DataFrame(pca.transform(x_test), index=x_test.index)

### ICA
n_components = x_train.shape[1]

ica = decomp.FastICA(n_components=n_components, whiten=True, 
                     max_iter=10000, random_state=42).fit(x_train)
x_train_ica = pd.DataFrame(ica.transform(x_train), index=x_train.index)
x_test_ica = pd.DataFrame(ica.transform(x_test), index=x_test.index)

model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train_ica, y_train, sample_weight)
model.feature_importances_

## Store ICA-transformed data set
i = 0

for column in x_train_ica:
    if model.feature_importances_[i] <= .01: # only include features with 
                                            # >.01 importance
        x_train_ica = x_train_ica.drop([column], axis=1)
        x_test_ica = x_test_ica.drop([column], axis=1)
    i += 1

## ICA Tree visualization
model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train_ica, y_train, sample_weight)

dot_data = tree.export_graphviz(model, out_file=None, max_depth=2, 
                     feature_names=x_train_ica.columns,  
                     class_names=y_train.values,  
                     filled=True, rounded=True,  
                     special_characters=True)  
graph = graphviz.Source(dot_data)  
graph 

### Randomized Projection
rp = sk_randproj.SparseRandomProjection(n_components=n_components, 
                                        random_state=42)
    
## CV baseline scores to compare full fature set vs randomized projections
skf = sk_modsel.StratifiedKFold(n_splits=5, random_state=42)
baseline = []

for train_idx, val_idx in skf.split(x_train, y_train):
    model = LogisticRegression(solver='liblinear', random_state=42, 
                               max_iter=10000)
    model.fit(x_train.iloc[train_idx], y_train.iloc[train_idx])
    baseline.append(metrics.accuracy_score(model.predict(
            x_train.iloc[val_idx]), y_train.iloc[val_idx]))
    
baseline = np.mean(baseline)

## loop over different projection sizes
accuracies = []
components = np.int32(np.arange(2, n_components))    

for comp in components:
    skf = sk_modsel.StratifiedKFold(n_splits=5, random_state=42)
    cv_acc = []
    
    for train_idx, val_idx in skf.split(x_train, y_train):
        # create the random projection
        rp = sk_randproj.SparseRandomProjection(n_components = comp, 
                                                random_state=42)
        x_train_rp = rp.fit_transform(x_train.iloc[train_idx])
     
        # train a classifier on the sparse random projection
        model = LogisticRegression(solver='liblinear', random_state=42, 
                                   max_iter=10000)
        model = model.fit(x_train_rp, y_train.iloc[train_idx])
     
        # evaluate the model and update the list of accuracies
        x_test_rp = rp.transform(x_train.iloc[val_idx])
        cv_acc.append(metrics.accuracy_score(model.predict(x_test_rp),
                                                 y_train.iloc[val_idx]))
        
    accuracies.append(np.mean(cv_acc))
        
## create the figure
plt.figure()
plt.suptitle("Sparse Projection Accuracy")
plt.xlabel("Number of Components")
plt.ylabel("Accuracy")
plt.xlim([2, n_components])
plt.ylim([.6, .75])
 
## plot the baseline and random projection accuracies
plt.plot(components, [baseline] * len(accuracies), color = "r")
plt.plot(components, accuracies)

plt.show() # Maximal CV accuracy at 24 components

## Store Randomly-Projected data set
rp = sk_randproj.SparseRandomProjection(n_components = 24, random_state=42)
x_train_rp = pd.DataFrame(rp.fit_transform(x_train), index=x_train.index)
x_test_rp = pd.DataFrame(rp.transform(x_test), index=x_test.index)

###################################################
## Data Set 1 - Task 3 - Clustering on reduced data
###################################################

### K-Means 

## K-Means: DT data
title = 'K-Means Elbow Test - DT-reduced Data'
km_search(x_train_dt, y_train, title) # Elbow at 2 components
model = KMeans(n_clusters=5, random_state=42).fit(x_train_dt)
title = 'K-Means Silhouette Plot - DT-reduced Data'
plot_silhouette(x_train, y_train, model, 5, title)

## K-Means: PCA data
title = 'K-Means Elbow Test - PCA-transformed Data'
km_search(x_train_pca, y_train, title) # Elbow at 5 clusters
model = KMeans(n_clusters=5, random_state=42).fit(x_train_pca)
title = 'K-Means Silhouette Plot - PCA-transformed Data'
plot_silhouette(x_train, y_train, model, 5, title)

## K-Means: ICA data
title = 'K-Means Elbow Test - ICA-transformed Data'
km_search(x_train_ica, y_train, title) # Elbow at 3 clusters
model = KMeans(n_clusters=5, random_state=42).fit(x_train_ica)
title = 'K-Means Silhouette Plot - ICA-transformed Data'
plot_silhouette(x_train, y_train, model, 5, title)

## K-Means: Random-Projection data
title = 'K-Means Elbow Test - Randomly-projected Data'
km_search(x_train_rp, y_train, title) # Elbow at 5 clusters
model = KMeans(n_clusters=5, random_state=42).fit(x_train_rp)
title = 'K-Means Silhouette Plot - Randomly-projected Data'
plot_silhouette(x_train, y_train, model, 5, title)

### Expectation-Maximization - 1 to 10 clusters

## Expectation-Maximization: DT data
title = 'EM BIC - DT-reduced Data'
em_search(x_train_dt, y_train, title) # Lowest BIC at 4 clusters
model = GM(n_components=2, random_state=42).fit(x_train_dt)
title = 'EM Silhouette Plot - DT-reduced Data'
plot_silhouette(x_train, y_train, model, 2, title)

## Expectation-Maximization: PCA data
title = 'EM BIC - PCA-transformed Data'
em_search(x_train_pca, y_train, title) # Lowest BIC at 2 clusters
model = GM(n_components=2, random_state=42).fit(x_train_pca)
title = 'EM Silhouette Plot - PCA-transformed Data'
plot_silhouette(x_train, y_train, model, 2, title)

## Expectation-Maximization: ICA data
title = 'EM BIC - ICA-transformed Data'
em_search(x_train_ica, y_train, title) # Lowest BIC at 2 clusters
model = GM(n_components=2, random_state=42).fit(x_train_ica)
title = 'EM Silhouette Plot - ICA-transformed Data'
plot_silhouette(x_train, y_train, model, 2, title)

## Expectation-Maximization: Random-Projection  data
title = 'EM BIC - Randomly-projected Data'
em_search(x_train_rp, y_train, title) # Lowest BIC at 1 cluster
model = GM(n_components=2, random_state=42).fit(x_train_rp)
title = 'EM Silhouette Plot - Randomly-projected Data'
plot_silhouette(x_train, y_train, model, 2, title)

############################################
# Data Set 1 - Task 4  - ANN on reduced data
############################################

### DT data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_dt.shape[1]+1,2)}
param_search(x_train_dt, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=5)
ann_eval(x_train_dt, y_train, x_test_dt, y_test, model)

### PCA data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_pca.shape[1]+1,2)}
param_search(x_train_pca, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=1)
ann_eval(x_train_pca, y_train, x_test_pca, y_test, model)

### ICA data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_ica.shape[1]+1,2)}
param_search(x_train_ica, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=23)
ann_eval(x_train_ica, y_train, x_test_ica, y_test, model)

### RP data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_rp.shape[1]+1,2)}
param_search(x_train_rp, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=3)
ann_eval(x_train_rp, y_train, x_test_rp, y_test, model)

##############################################
# Data Set 1 - Task 5  - ANN on clustered data
##############################################

### KM data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_km.shape[1]+1)}
param_search(x_train_km, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1, 
                      hidden_layer_sizes=4)
ann_eval(x_train_km, y_train, x_test_km, y_test, model)

title = 'ANN Validation Curve: 1 hidden layer'
plot_validation_curve(x_train_km, y_train, model, title,
                      param_name='hidden_layer_sizes', 
                      param_range=np.arange(1,x_train_km.shape[1]+1))  

### EM data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,10)}
param_search(x_train_em, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=1)
ann_eval(x_train_em, y_train, x_test_em, y_test, model)

# Validation curves: hidden_layer_sizes, alpha
title = 'ANN Validation Curve: 1 hidden layer'
plot_validation_curve(x_train, y_train, model, title,
                      param_name='hidden_layer_sizes', 
                      param_range=np.arange(1,11))      

### Original data
model = MLPClassifier(max_iter=10000, random_state=12345, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1, 
                      hidden_layer_sizes=5, alpha=.1)
ann_eval(x_train, y_train, x_test, y_test, model)

###############################################################################
#                               Analyses: Data Set 2                          #
###############################################################################

############################
# Data Set 2 - Preprocessing
############################

### Data set import and exploratory analysis
context2 = pd.read_csv('sportsArticles.csv', index_col='TextID')
context2.head()
context2.describe()
context2 = context2.drop(['URL'], axis=1)
print('Data Set 2')

### Preprocess
x_train, x_test, y_train, y_test = prep(context2.drop(['Label'], axis=1), 
                                        context2['Label'], random_state=42, 
                                        scaler=sk_prep.MaxAbsScaler(),
                                        test_size=.3, 
                                        stratify=context2['Label'])

sample_weight = np.array([1 if i == 'Objective' else 2 for i in y_train])
 
######################################################
# Data Set 2 - Task 1 - Clustering on full feature set
######################################################

### K-Means - 1 to 15 clusters
km_search(x_train, y_train) # Elbow at 2 clusters  

## Store KM-clustered data set
model = KMeans(n_clusters=2, random_state=42)
model = model.fit(x_train)

x_train_km = pd.DataFrame(model.labels_, index=x_train.index)
x_train_km = pd.get_dummies(x_train_km[0])
x_train_km = x_train_km.drop([1], axis=1)
x_test_km = pd.DataFrame(model.predict(x_test), index=x_test.index)
x_test_km = pd.get_dummies(x_test_km[0])
x_test_km = x_test_km.drop([1], axis=1)

title = 'K-Means Silhouette Plot'
plot_silhouette(x_train, y_train, model, 2, title)

### Expectation-Maximization - 1 to 10 clusters
em_search(x_train, y_train) # Lowest BIC at 2 clusters

## Store EM-clustered data set
model = GM(n_components=2, random_state=42)
model = model.fit(x_train)

x_train_em = pd.DataFrame(model.predict(x_train))
x_test_em = pd.DataFrame(model.predict(x_test))

title = 'Expectation Maximization Silhouette Plot'
plot_silhouette(x_train, y_train, model, 2, title)


################################################
# Data Set 2 - Task 2 - Dimensionality Reduction
################################################

### Decision Tree
model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train, y_train, sample_weight)
model.feature_importances_

## Store DT-reduced data set
x_train_dt = x_train.copy()
x_test_dt = x_test.copy()

i = 0

for column in x_train:
#    print(column + ' ' + str(model.feature_importances_[i]))
    if model.feature_importances_[i] <= .01: # only include features with 
                                            # >.01 importance
        x_train_dt = x_train_dt.drop([column], axis=1)
        x_test_dt = x_test_dt.drop([column], axis=1)
    i += 1

## Tree visualization
model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train_dt, y_train, sample_weight)

dot_data = tree.export_graphviz(model, out_file=None, max_depth=2, 
                     feature_names=x_train_dt.columns,  
                     class_names=y_train.values,  
                     filled=True, rounded=True,  
                     special_characters=True)  
graph = graphviz.Source(dot_data)  
graph 

### PCA
pca = decomp.PCA(random_state=42).fit(x_train)

plt.plot(np.cumsum(pca.explained_variance_ratio_), 'b--')
plt.xlabel('Number of Components')
plt.ylabel('Variance (%)') #for each component
plt.title('Explained Variance')
plt.show() # 18 components preserves >= 90% of variance
print(np.cumsum(pca.explained_variance_ratio_))

## Store PCA-transformed data set
pca = decomp.PCA(n_components=18, random_state=42).fit(x_train)
x_train_pca = pd.DataFrame(pca.transform(x_train), index=x_train.index)
x_test_pca = pd.DataFrame(pca.transform(x_test), index=x_test.index)

### ICA
n_components = x_train.shape[1]

ica = decomp.FastICA(n_components=n_components, whiten=True, 
                     max_iter=10000, tol=.01, random_state=42).fit(x_train)
x_train_ica = pd.DataFrame(ica.transform(x_train), index=x_train.index)
x_test_ica = pd.DataFrame(ica.transform(x_test), index=x_test.index)

model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train_ica, y_train, sample_weight)
model.feature_importances_

## Store ICA-transformed data set
i = 0

for column in x_train_ica:
    if model.feature_importances_[i] <= .01: # only include features with 
                                            # >.01 importance
        x_train_ica = x_train_ica.drop([column], axis=1)
        x_test_ica = x_test_ica.drop([column], axis=1)
    i += 1

## ICA Tree visualization
model = tree.DecisionTreeClassifier(random_state=42)
model = model.fit(x_train_ica, y_train, sample_weight)

dot_data = tree.export_graphviz(model, out_file=None, max_depth=2, 
                     feature_names=x_train_ica.columns,  
                     class_names=y_train.values,  
                     filled=True, rounded=True,  
                     special_characters=True)  
graph = graphviz.Source(dot_data)  
graph 

### Randomized Projection
rp = sk_randproj.SparseRandomProjection(n_components=n_components, 
                                        random_state=42)
    
## CV baseline scores to compare full fature set vs randomized projections
skf = sk_modsel.StratifiedKFold(n_splits=5, random_state=42)
baseline = []

for train_idx, val_idx in skf.split(x_train, y_train):
    model = LogisticRegression(solver='liblinear', random_state=42, 
                               max_iter=10000)
    model.fit(x_train.iloc[train_idx], y_train.iloc[train_idx])
    baseline.append(metrics.accuracy_score(model.predict(
            x_train.iloc[val_idx]), y_train.iloc[val_idx]))
    
baseline = np.mean(baseline)

## loop over different projection sizes
accuracies = []
components = np.int32(np.arange(2, n_components))    

for comp in components:
    skf = sk_modsel.StratifiedKFold(n_splits=5, random_state=42)
    cv_acc = []
    
    for train_idx, val_idx in skf.split(x_train, y_train):
        # create the random projection
        rp = sk_randproj.SparseRandomProjection(n_components = comp, 
                                                random_state=42)
        x_train_rp = rp.fit_transform(x_train.iloc[train_idx])
     
        # train a classifier on the sparse random projection
        model = LogisticRegression(solver='liblinear', random_state=42, 
                                   max_iter=10000)
        model = model.fit(x_train_rp, y_train.iloc[train_idx])
     
        # evaluate the model and update the list of accuracies
        x_test_rp = rp.transform(x_train.iloc[val_idx])
        cv_acc.append(metrics.accuracy_score(model.predict(x_test_rp),
                                                 y_train.iloc[val_idx]))
        
    accuracies.append(np.mean(cv_acc))
        
## create the figure
plt.figure()
plt.suptitle("Sparse Projection Accuracy")
plt.xlabel("Number of Components")
plt.ylabel("Accuracy")
plt.xlim([2, n_components])
plt.ylim([.78, .83])
 
## plot the baseline and random projection accuracies
plt.plot(components, [baseline] * len(accuracies), color = "r")
plt.plot(components, accuracies)

plt.show() # Maximal CV accuracy at 44 components

## Store Randomly-Projected data set
rp = sk_randproj.SparseRandomProjection(n_components = 44, random_state=42)
x_train_rp = pd.DataFrame(rp.fit_transform(x_train), index=x_train.index)
x_test_rp = pd.DataFrame(rp.transform(x_test), index=x_test.index)

###################################################
## Data Set 2 - Task 3 - Clustering on reduced data
###################################################

### K-Means 

## K-Means: DT data
title = 'K-Means Elbow Test - DT-reduced Data'
km_search(x_train_dt, y_train, title) # Elbow at 2 components
model = KMeans(n_clusters=2, random_state=42).fit(x_train_dt)
title = 'K-Means Silhouette Plot - DT-reduced Data'
plot_silhouette(x_train, y_train, model, 2, title)

## K-Means: PCA data
title = 'K-Means Elbow Test - PCA-transformed Data'
km_search(x_train_pca, y_train, title) # Elbow at 2 clusters
model = KMeans(n_clusters=2, random_state=42).fit(x_train_pca)
title = 'K-Means Silhouette Plot - PCA-transformed Data'
plot_silhouette(x_train, y_train, model, 2, title)

## K-Means: ICA data
title = 'K-Means Elbow Test - ICA-transformed Data'
km_search(x_train_ica, y_train, title) # Elbow at 3 clusters
model = KMeans(n_clusters=2, random_state=42).fit(x_train_ica)
title = 'K-Means Silhouette Plot - ICA-transformed Data'
plot_silhouette(x_train, y_train, model, 2, title)

## K-Means: Random-Projection data
title = 'K-Means Elbow Test - Randomly-projected Data'
km_search(x_train_rp, y_train, title) # Elbow at 2 or 3 clusters
model = KMeans(n_clusters=2, random_state=42).fit(x_train_rp)
title = 'K-Means Silhouette Plot - Randomly-projected Data'
plot_silhouette(x_train, y_train, model, 2, title)

### Expectation-Maximization - 1 to 10 clusters

## Expectation-Maximization: DT data
title = 'EM BIC - DT-reduced Data'
em_search(x_train_dt, y_train, title) # Lowest BIC at 3 clusters
model = GM(n_components=2, random_state=42).fit(x_train_dt)
title = 'EM Silhouette Plot - DT-reduced  Data'
plot_silhouette(x_train, y_train, model, 2, title)

## Expectation-Maximization: PCA data
title = 'EM BIC - PCA-transformed Data'
em_search(x_train_pca, y_train, title) # Lowest BIC at 7 clusters
model = GM(n_components=2, random_state=42).fit(x_train_pca)
title = 'EM Silhouette Plot - PCA-transformed  Data'
plot_silhouette(x_train, y_train, model, 2, title)

## Expectation-Maximization: ICA data
title = 'EM BIC - ICA-transformed Data'
em_search(x_train_ica, y_train, title) # Lowest BIC at 2 clusters
model = GM(n_components=2, random_state=42).fit(x_train_ica)
title = 'EM Silhouette Plot - ICA-transformed  Data'
plot_silhouette(x_train, y_train, model, 2, title)

## Expectation-Maximization: Random-Projection  data
title = 'EM BIC - Randomly-projected Data'
em_search(x_train_rp, y_train, title) # Lowest BIC at 1 cluster
model = GM(n_components=2, random_state=42).fit(x_train_rp)
title = 'EM Silhouette Plot - Randomly-projected  Data'

############################################
# Data Set 2 - Task 4  - ANN on reduced data
############################################

### DT data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_dt.shape[1]+1,2)}
param_search(x_train_dt, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=19)
ann_eval(x_train_dt, y_train, x_test_dt, y_test, model)

### PCA data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_pca.shape[1]+1,2)}
param_search(x_train_pca, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=3)
ann_eval(x_train_pca, y_train, x_test_pca, y_test, model)

### ICA data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_ica.shape[1]+1,2)}
param_search(x_train_ica, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=27)
ann_eval(x_train_ica, y_train, x_test_ica, y_test, model)

### RP data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_rp.shape[1]+1,2)}
param_search(x_train_rp, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=9)
ann_eval(x_train_rp, y_train, x_test_rp, y_test, model)

##############################################
# Data Set 2 - Task 5  - ANN on clustered data
##############################################

### KM data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,x_train_km.shape[1]+1)}
param_search(x_train_km, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1, 
                      hidden_layer_sizes=1)
ann_eval(x_train_km, y_train, x_test_km, y_test, model)

title = 'ANN Validation Curve: 1 hidden layer'
plot_validation_curve(x_train_km, y_train, model, title,
                      param_name='hidden_layer_sizes', 
                      param_range=np.arange(1,11))  

### EM data
model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      warm_start=True)
params = {'hidden_layer_sizes': np.arange(1,10)}
param_search(x_train_em, y_train, model=model, params=params, 
             search_type='grid', random_state=42, n_top=10)

model = MLPClassifier(max_iter=10000, random_state=42, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.01, 
                      hidden_layer_sizes=1)
ann_eval(x_train_em, y_train, x_test_em, y_test, model)

# Validation curves: hidden_layer_sizes, alpha
title = 'ANN Validation Curve: 1 hidden layer'
plot_validation_curve(x_train, y_train, model, title,
                      param_name='hidden_layer_sizes', 
                      param_range=np.arange(1,11))      
 
### Original data
model = MLPClassifier(max_iter=10000, random_state=12345, solver='sgd',
                      learning_rate='adaptive', learning_rate_init=.1,
                      hidden_layer_sizes=25, alpha=.001)
ann_eval(x_train, y_train, x_test, y_test, model)


###############################################################################
#                                       Runtime                               #
###############################################################################

print('Runtime: %.2f seconds.'
      % (time() - init))