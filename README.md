Required Libraries:

- pandas
- numpy
- scikitlearn
- matplotlib
- itertools
- scipy

Description: 

The attached Python code is an analysis of the “Student Performance” and “Sports
articles for objectivity analysis” data sets from the UCI Machine Learning 
Repository. Specifically, it implements and evaluates K-Means, Gaussian Mixtures 
(an implementation of Expectation Maximization clustering), Decision Tree 
feature selection, PCA, ICA, and Sparse Random Projections (an implementation of 
Random Projections for sparse matrices) from scikit-learn. Each of the clustered 
and dimensionally-reduced data sets were also run through scikit-learn’s neural 
network implementation known as MLPClassifier.

How to run:

To run, ensure that the sportsArticles.csv and student-mat.csv files are in the 
same directory as the assignment4.py file, then run the .py file.